/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author Martin Pilar <mpilarcastillejo@gmail.com>
 */
public class MainReporteDemo {

    public static void main(String[] args) {
        obtenerReporte8();
    }

    public static JasperPrint obtenerReporte8() {
        Collection<Map<String, Object>> listaData = obtenerData(obtenerDataServicio());
        Map<String, Integer> listaCabezera = obtenerListaCabezera(obtenerDataServicio());
        UtilReportWeb reporteUtil = new UtilReportWeb(listaCabezera, listaData, null, null);
        int indiceTotal = listaCabezera.size() - 1;
        int indiceCategoria = 1;
        reporteUtil.ConstruirReportePDF(true, "Grafico", "Categoria", "Serie", indiceCategoria, indiceTotal, PlotOrientation.HORIZONTAL);
        reporteUtil.agruparColumnaPorFila(reporteUtil.estiloFila(), indiceCategoria, indiceTotal);
        JasperViewer.viewReport(reporteUtil.generarReporte());
        return reporteUtil.generarReporte();
    }

    public static JasperPrint unirReporte(JasperPrint jasperPrintBase, JasperPrint... jasperPrints) {
        for (JasperPrint jasperPrint1 : jasperPrints) {
            for (int j = 0; j < jasperPrint1.getPages().size(); j++) {
                jasperPrintBase.addPage(jasperPrint1.getPages().get(j));
            }
        }
        return jasperPrintBase;
    }

    private static Map<String, String> obtenerParametros(String title) {
        final Map<String, String> parameters = new HashMap();
        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
//        URL urllogo = getClass().getResource("/logo-mimp.jpg");
        parameters.put("titulo", title);
        parameters.put("logo", pathLogo);
        return parameters;
    }

    private static List<ReSupervHrtOut> obtenerDataServicio() {
        List<ReSupervHrtOut> reporte8Lista = new ArrayList();
        String nombre = "Razon";
        for (Integer i = 0; i < 20; i++) {
            List<ReSupervisionesAnioOut> anios = new ArrayList();
            for (Integer j = 0; j < 10; j++) {
                anios.add(new ReSupervisionesAnioOut("201" + j, i * 2));
            }
            ReSupervHrtOut temp = new ReSupervHrtOut(i, i, nombre, anios, new BigInteger("4"));
            reporte8Lista.add(temp);
            if (i % 5 == 0) {
                nombre = nombre + i;
            }
        }
        return reporte8Lista;
    }

    private static Collection<Map<String, Object>> obtenerData(List<ReSupervHrtOut> reporte8Lista) {
        List<Map<String, Object>> data = new ArrayList<>();
        reporte8Lista.stream().map((supe) -> {
            Map<String, Object> dat = new HashMap<>();
            dat.put("CODIGO", supe.getIdHrt());
            dat.put("HRT", supe.getRazonSocial());
            supe.getSupervisionesPorAnio().forEach((nombreAnio) -> {
                dat.put(nombreAnio.getAnio(), nombreAnio.getCantSupervisiones());
            });
            dat.put("TOTAL", supe.getTotalSuperv());
            return dat;
        }).forEachOrdered((dat) -> {
            data.add(dat);
        });
        return data;
    }

    private static Map<String, Integer> obtenerListaCabezera(List<ReSupervHrtOut> tempListaSupervision) {
        Map<String, Integer> listaCabezera = new LinkedHashMap();
        for (ReSupervHrtOut supe : tempListaSupervision) {
            listaCabezera.put("CODIGO", 40);
            listaCabezera.put("HRT", 100);
            supe.getSupervisionesPorAnio().forEach((nombreAnio) -> {
                listaCabezera.put(nombreAnio.getAnio(), 20);
            });
            listaCabezera.put("TOTAL", 20);
            break;
        }
        return listaCabezera;
    }

    private static int obtenerColumnasAgrupar(List<ReSupervHrtOut> reporte8Lista) {
        return reporte8Lista.get(0).getSupervisionesPorAnio().size();
    }

}
