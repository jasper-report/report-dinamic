/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Martin Pilar <mpilarcastillejo@gmail.com>
 */
public class MainBuilder {

    public static void main(String[] args) {
        try {
            List<ReSupervHrtOut> reporte8Lista = obtenerDataServicio();
            Collection<Map<String, Object>> listaData = obtenerData(reporte8Lista);
            List<String> listaCabezera = obtenerListaCabezera(reporte8Lista);
            Map<String, String> params = obtenerParametros("Reporte 8");
            UtilReportBuilder report = new UtilReportBuilder(listaCabezera, listaData, params);
            report.ConstruirReporte(false, "Grafico", "Categoria", "Serie");
            report.builder.setColumnSpace(250);
            report.generarReporte();
            report.exportToExcel(report.jp);
            JasperViewer.viewReport(report.jp);
        } catch (Exception ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Map<String, String> obtenerParametros(String title) {
        final Map<String, String> parameters = new HashMap();
        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
//        URL urllogo = getClass().getResource("/logo-mimp.jpg");
//        parameters.put("titulo", title);
        parameters.put("logo", pathLogo);
        return parameters;
    }

    private static List<ReSupervHrtOut> obtenerDataServicio() {
        List<ReSupervHrtOut> reporte8Lista = new ArrayList();
        for (Integer i = 0; i < 5; i++) {
            List<ReSupervisionesAnioOut> anios = new ArrayList();
            for (Integer j = 0; j < 10; j++) {
                anios.add(new ReSupervisionesAnioOut("201" + j, i));
            }
            ReSupervHrtOut temp = new ReSupervHrtOut(i, i * 5, "razon social crece segun la data completa" + i, anios, new BigInteger("3"));
            reporte8Lista.add(temp);
        }
        return reporte8Lista;
    }

    private static Collection<Map<String, Object>> obtenerData(List<ReSupervHrtOut> reporte8Lista) {
        List<Map<String, Object>> data = new ArrayList<>();
        String cod = "Codi";
        int numero = 0;
        for (ReSupervHrtOut supe : reporte8Lista) {
            Map<String, Object> dat = new HashMap<>();
            dat.put("CODIGO ID", cod);
            dat.put("HRT", supe.getRazonSocial());
            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
                dat.put(nombreAnio.getAnio(), nombreAnio.getCantSupervisiones());
            }
            dat.put("TOTAL", supe.getTotalSuperv());
            data.add(dat);
            if (numero % 2 == 0) {
                cod = cod + numero;
            }
            numero++;
        }
        return data;
    }

    private static List<String> obtenerListaCabezera(List<ReSupervHrtOut> tempListaSupervision) {
        List<String> listaCabezera = new ArrayList<>();
        for (ReSupervHrtOut supe : tempListaSupervision) {
            listaCabezera.add("CODIGO ID");
            listaCabezera.add("HRT");
            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
                listaCabezera.add(nombreAnio.getAnio());
            }
            listaCabezera.add("TOTAL");
            break;
        }
        return listaCabezera;
    }

}
