/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

/**
 *
 * @author calidad
 */
public class ReSupervisionesAnioOut {

    private String anio;
    private Integer cantSupervisiones;

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public Integer getCantSupervisiones() {
        return cantSupervisiones;
    }

    public void setCantSupervisiones(Integer cantSupervisiones) {
        this.cantSupervisiones = cantSupervisiones;
    }

    public ReSupervisionesAnioOut(String anio, Integer cantSupervisiones) {
        this.anio = anio;
        this.cantSupervisiones = cantSupervisiones;
    }

}
