/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.LayoutManager;
import ar.com.fdvs.dj.core.layout.ListLayoutManager;
import ar.com.fdvs.dj.domain.BooleanExpression;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.chart.DJChart;
import ar.com.fdvs.dj.domain.chart.DJChartOptions;
import ar.com.fdvs.dj.domain.chart.builder.DJBarChartBuilder;
import ar.com.fdvs.dj.domain.chart.plot.DJAxisFormat;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.GroupLayout;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import ar.com.fdvs.dj.util.SortUtils;
import static com.autentia.tutoriales.dynamicjasper.ReportBase.log;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author Martin Pilar <mpilarcastillejo@gmail.com>
 */
public class UtilReportBuilder {

    private List<String> listaCabezera = new ArrayList<>();
    private Map params = new HashMap();
    private Collection<Map<String, Object>> data = new ArrayList<>();
    public DynamicReport dr;
    public JasperPrint jp;
    public JasperReport jr;
    public DynamicReportBuilder builder;

    public UtilReportBuilder(List<String> listaCabezera, Collection<Map<String, Object>> data, Map<String, String> params) {
        this.listaCabezera = listaCabezera;
        this.data = data;
        this.params = params;
    }

    public void ConstruirReporte(boolean conGrafico, String titulo, String categoria, String serie) {
        try {
            builder = obtenerReporte(conGrafico, titulo, categoria, serie);
        } catch (Exception ex) {
            Logger.getLogger(UtilReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generarReporte() {
        try {
            dr = builder.build();
            JRDataSource ds = getData();
            jr = DynamicJasperHelper.generateJasperReport(dr, getLayoutManager(), params);
//            jp = JasperFillManager.fillReport(jr, params, ds);
            if (ds != null) {
                jp = JasperFillManager.fillReport(jr, params, ds);
            } else {
                jp = JasperFillManager.fillReport(jr, params);
                log.debug("Filling done!");
                log.debug("Exporting the report (pdf, xls, etc)");
            }
        } catch (Exception ex) {
            Logger.getLogger(UtilReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void exportToJRXML() throws Exception {
        if (jr != null) {
            DynamicJasperHelper.generateJRXML(jr, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".jrxml");
        } else {
            DynamicJasperHelper.generateJRXML(dr, this.getLayoutManager(), this.params, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".jrxml");
        }
    }

    private void exportReport() throws Exception {

        final String path = System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".pdf";

        log.debug("Exporing report to: " + path);
        JRPdfExporter exporter = new JRPdfExporter();
        File outputFile = new File(path);
        File parentFile = outputFile.getParentFile();
        if (parentFile != null) {
            parentFile.mkdirs();
        }

        FileOutputStream fos = new FileOutputStream(outputFile);

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
        exporter.exportReport();
        log.debug("Report exported: " + path);

    }

    private LayoutManager getLayoutManager() {
        return new ListLayoutManager();
    }

    public Style obtenerEstilo() {
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        Color c = new Color(225, 35, 40);
        styleColumns.setTextColor(Color.WHITE);
        styleColumns.setBackgroundColor(c);
        styleColumns.setBorderColor(Color.WHITE);
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setBorder(Border.THIN());
        styleColumns.setVerticalAlign(VerticalAlign.MIDDLE);
        styleColumns.setTransparent(false);
        return styleColumns;
    }

    public Style estiloFila() {
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        Color c = new Color(225, 35, 40);
//        styleColumns.setTextColor(Color.WHITE);
        styleColumns.setBackgroundColor(Color.DARK_GRAY);
//        styleColumns.setTransparent(false);
//        styleColumns.setBorderColor(Color.WHITE);
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setBorder(Border.THIN());
//        styleColumns.set
//        styleColumns.setVerticalAlign(VerticalAlign.MIDDLE);
//        styleColumns.setTransparent(false);
        return styleColumns;
    }

    public DynamicReportBuilder obtenerReporte(boolean conGrafico, String titulo, String categoria, String serie) throws Exception {
        DynamicReportBuilder report = new DynamicReportBuilder();
        report.setTitle("EQUIPOS");
        report.setMargins(1, 1, 1, 1);
        report.setTemplateFile("reporte8.jrxml");

        for (String cabezera : listaCabezera) {
            AbstractColumn c1 = ColumnBuilder.getInstance()
                    .setColumnProperty(cabezera, Object.class.getName())
                    .setTitle(cabezera)
                    .setWidth(150)
                    .setFixedWidth(false)
                    .setStyle(estiloFila())
                    //                    .setPrintRepeatedValues(false)
                    .setHeaderStyle(obtenerEstilo())
                    .build();
            report.addColumn(c1);
        }
        report.setUseFullPageWidth(true);
        report.setPrintColumnNames(true);
        report.setIgnorePagination(true); // para excel una sola pagina (hoja)
//        report.setColspan(indiceInicialAgrupar, cantidadAgrupar, "A�O");
        Style detailStyle = new Style();

        Style headerStyle = new Style();
        headerStyle.setFont(Font.ARIAL_MEDIUM_BOLD);
        headerStyle.setBorderBottom(Border.PEN_1_POINT());
        headerStyle.setBackgroundColor(Color.gray);
        headerStyle.setTextColor(Color.white);
        headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
        headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
        headerStyle.setTransparency(Transparency.OPAQUE);

        Style headerVariables = new Style();
        headerVariables.setFont(Font.ARIAL_MEDIUM_BOLD);
        //		headerVariables.setBorderBottom(Border.THIN());
        headerVariables.setHorizontalAlign(HorizontalAlign.RIGHT);
        headerVariables.setVerticalAlign(VerticalAlign.MIDDLE);

        Style titleStyle = new Style();
        titleStyle.setFont(new Font(18, Font._FONT_VERDANA, true));
        Style importeStyle = new Style();
        importeStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
        Style oddRowStyle = new Style();
        oddRowStyle.setBorder(Border.NO_BORDER());
        oddRowStyle.setBackgroundColor(Color.LIGHT_GRAY);
        oddRowStyle.setTransparency(Transparency.OPAQUE);
//        report.setIgnorePagination(true); // para excel una sola pagina (hoja)
//        report.setColspan(2, 2, "Union por fin");
//        grafico(report, PlotOrientation.VERTICAL);
//        rb.setPrintColumnNames(true); //Show the column names on the top of every page
        GroupBuilder gb1 = new GroupBuilder();
        DJGroup g1 = gb1.setCriteriaColumn((PropertyColumn) report.getColumn(0))
                .addHeaderVariable(report.getColumn(0), DJCalculation.COUNT, headerVariables) // tell the group place a variable footer of the column "columnAmount" with the SUM of allvalues of the columnAmount in this group.
                .setGroupLayout(GroupLayout.VALUE_IN_HEADER_WITH_HEADERS) // tells the group how to be shown, there are manyposibilities, see the GroupLayout for more.
                .build();

        GroupBuilder gb2 = new GroupBuilder(); // Create another group (using another column as criteria)
        
        CustomExpression ce = new BooleanExpression() {
            @Override
            public Object evaluate(Map map, Map map1, Map map2) {
                int suma = 0;
                for (Object object : map2.entrySet()) {
                    java.lang.Object key = object;
                    Integer s = (Integer) key;
                    suma = suma + s;
                }
                return suma;
                //To change body of generated methods, choose Tools | Templates.
            }
        };

        DJGroup g2 = gb1.setCriteriaColumn((PropertyColumn) report.getColumn(1))
                .addFooterVariable(report.getColumn(11), ce, headerVariables) // tell the group place a variable footer of the column "columnAmount" with the SUM of allvalues of the columnAmount in this group.
                .addFooterVariable(report.getColumn(12), ce, headerVariables) // idem for the columnaQuantity column
                .setGroupLayout(GroupLayout.VALUE_IN_HEADER) // tells the group how to be shown, there are manyposibilities, see the GroupLayout for more.
                .build();
        report.addGroup(g2);
        if (conGrafico) {
            report = grafico(report, PlotOrientation.VERTICAL, titulo, categoria, serie);
        }
        return report;
    }

    public void exportToExcel(JasperPrint jp) throws Exception {

//Creamos una instancia del objeto JRXlsExplorer que se encuentra en la librer�a jasperreports-X.X.X.jar
        JRXlsExporter exporter = new JRXlsExporter();
//Si queremos definir varios Sheet lo hacemos en un array de cadenas que posteriormente la a�adiremos por par�metros.
        String[] sheetNames = {"Hoja 1"};

//Creamos el fichero de salida donde exportaremos el informe a Excel
        File outputFile = new File("D://" + "exportacionEXCEL.xls");
        FileOutputStream fos = new FileOutputStream(outputFile);

//Empezamos a definir los par�metros de la exportacion
//Indicamos el objeto JasperPrint que deseamos exportar
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
//Indicamos el fichero donde vamos a exportar el informe
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos); //and output stream

        exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
//        exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.FALSE);
//        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
        exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
//        exporter.setParameter(JRXlsExporterParameter.IS_AUTO_DETECT_CELL_TYPE, Boolean.TRUE);
//Excel specific parameter
//Indicamos si queremos una p�gina por Sheet
        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
//Indicamos si deseamos eliminar los espacios vac�os entre filas
//        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
//Indicamos si quremos mostrar una p�gina en blanco como fondo
        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

//Definimos los nombres de los Sheet que es el Array de String que definimos arriba
        exporter.setParameter(JRXlsExporterParameter.SHEET_NAMES, sheetNames);

//Usamos la funci�n exportReport para crear el nuevo Excel.
        exporter.exportReport();
        log.debug("Report exported: " + System.getProperty("user.dir") + "/src/main/resources" + "exportacionEXCEL.xls");
    }

    public DynamicReportBuilder grafico(DynamicReportBuilder drb, PlotOrientation posicion, String titulo, String categoria, String serie) {
        DJAxisFormat categoryAxisFormat = new DJAxisFormat(categoria);
        categoryAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelMask("");
        categoryAxisFormat.setLineColor(Color.DARK_GRAY);

        DJAxisFormat valueAxisFormat = new DJAxisFormat(serie);
        valueAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelMask("#,##0.0");
        valueAxisFormat.setLineColor(Color.DARK_GRAY);

        DJChart djChart = new DJBarChartBuilder()
                //chart     
                .setX(20)
                .setY(10)
                //                .setWidth(500)
                .setHeight(300)
                .setCentered(false)
                .setBackColor(Color.LIGHT_GRAY)
                .setShowLegend(false)
                .setPosition(DJChartOptions.POSITION_FOOTER)
                .setTitle(titulo)
                .setTitleColor(Color.DARK_GRAY)
                .setTitleFont(Font.ARIAL_BIG_BOLD)
                //                .setSubtitle("subtitle")
                .setSubtitleColor(Color.DARK_GRAY)
                .setSubtitleFont(Font.COURIER_NEW_BIG_BOLD)
                .setLegendColor(Color.DARK_GRAY)
                .setLegendFont(Font.COURIER_NEW_MEDIUM_BOLD)
                .setLegendBackgroundColor(Color.WHITE)
                .setLegendPosition(DJChartOptions.EDGE_BOTTOM)
                .setTitlePosition(DJChartOptions.EDGE_TOP)
                .setLineStyle(DJChartOptions.LINE_STYLE_DOTTED)
                .setLineWidth(1)
                .setLineColor(Color.DARK_GRAY)
                .setPadding(5)
                //dataset
                .setOrientation(posicion)
                .setCategory((PropertyColumn) drb.getColumn(1))
                .addSerie(drb.getColumn(12))
                //plot
                //                .setXOffset(10)
                //                .setyOffset(10)
                .setShowLabels(false)
                .setCategoryAxisFormat(categoryAxisFormat)
                .setValueAxisFormat(valueAxisFormat)
                .build();
        drb.addChart(djChart);
        return drb;
    }

    public JRDataSource getData() {
        Collection<Map<String, Object>> dataOneReport = data;
        dataOneReport = SortUtils.sortCollection(dataOneReport, dr.getColumns());
        //Create a JRDataSource, the Collection used
        return new JRBeanCollectionDataSource(dataOneReport);
    }

    public Map<String, Object> obtenerParametros(String title) {
        final Map<String, Object> parameters = new HashMap();
//        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
        URL urllogo = getClass().getResource("/logo-mimp.jpg");
        parameters.put("titulo", title);
        parameters.put("logo", urllogo.toString());
        return parameters;
    }

}
