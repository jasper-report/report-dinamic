/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Martin Pilar <mpilarcastillejo@gmail.com>
 */
public class main {

    public static void main(String[] args) {
        try {
            List<ReSupervHrtOut> reporte8Lista = obtenerDataServicio();
            Collection<Map<String, Object>> listaData = obtenerData(reporte8Lista);
            List<String> listaCabezera = obtenerListaCabezera(reporte8Lista);
            Map<String, String> params = obtenerParametros("fusionar celdas es rso, el texto dentro de un campo que est� anidado dentro de una");
            UtilReport report = new UtilReport(listaCabezera, listaData, params);
            report.ConstruirReporte(false, "Grafico", "Categoria", "Serie");
//            report.agruparColumnaPorFila(report.obtenerEstilo(), 1, 12);
            JasperPrint jasper = report.generarReporte();
//            JasperPrint jasper = report.graficoCalculado(getDataGraficoCalculado(), "titulo", "CATEGORIA", "SERIE");
//            unirReporte(report.jp, reptCaluclado);      
            JasperViewer.viewReport(jasper);
        } catch (Exception ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static List<Map<String, Object>> getDataGraficoCalculado() {
        List<Map<String, Object>> data = new ArrayList<>();
        String cod = "categoria";
        for (int i = 0; i < 5; i++) {
            Map<String, Object> tempDAta = new LinkedHashMap<>();
            tempDAta.put("CATEGORIA", cod + i);
            Integer numero = 20;
            tempDAta.put("SERIE", new Integer(numero));
            numero++;
            data.add(tempDAta);
        }
        return data;
    }

    public static JasperPrint unirReporte(JasperPrint jasperPrintBase, JasperPrint... jasperPrints) {
        for (JasperPrint jasperPrint1 : jasperPrints) {
            for (int j = 0; j < jasperPrint1.getPages().size(); j++) {
                jasperPrintBase.addPage(jasperPrint1.getPages().get(j));
            }
        }
        return jasperPrintBase;
    }

    private static Map<String, String> obtenerParametros(String title) {
        final Map<String, String> parameters = new HashMap();
        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
//        URL urllogo = getClass().getResource("/logo-mimp.jpg");
//        parameters.put("titulo", title);
//        parameters.put("logo", pathLogo);
        return parameters;
    }

    private static List<ReSupervHrtOut> obtenerDataServicio() {
        List<ReSupervHrtOut> reporte8Lista = new ArrayList();
        String nombre = "Razon";
        for (Integer i = 0; i < 20; i++) {
            List<ReSupervisionesAnioOut> anios = new ArrayList();
            for (Integer j = 0; j < 10; j++) {
                anios.add(new ReSupervisionesAnioOut("201" + j, (i * 2)));
            }
            String valor = String.valueOf(i * 8);
            ReSupervHrtOut temp = new ReSupervHrtOut(i, i, nombre, anios, new BigInteger(valor));
            reporte8Lista.add(temp);
            if (i % 5 == 0) {
                nombre = nombre + i;
            }
        }
        return reporte8Lista;
    }

    private static Collection<Map<String, Object>> obtenerData(List<ReSupervHrtOut> reporte8Lista) {
        List<Map<String, Object>> data = new ArrayList<>();
        String cod = "Codi";
        int numero = 0;
        for (ReSupervHrtOut supe : reporte8Lista) {
            Map<String, Object> dat = new HashMap<>();
            dat.put("CODIGO ID", numero);
            dat.put("HRT", supe.getRazonSocial());
            supe.getSupervisionesPorAnio().forEach((nombreAnio) -> {
                dat.put(nombreAnio.getAnio(), nombreAnio.getCantSupervisiones());
            });
            dat.put(" TOTAL", supe.getTotalSuperv());
            data.add(dat);
            if (numero % 2 == 0) {
                cod = cod + numero;
            }
            numero++;
        }
        return data;
    }

    private static List<String> obtenerListaCabezera(List<ReSupervHrtOut> tempListaSupervision) {
        List<String> listaCabezera = new ArrayList<>();
        for (ReSupervHrtOut supe : tempListaSupervision) {
            listaCabezera.add("CODIGO ID");
            listaCabezera.add("HRT");
            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
                listaCabezera.add(nombreAnio.getAnio());
            }
            listaCabezera.add("TOTAL");
            break;
        }
        return listaCabezera;
    }

}
