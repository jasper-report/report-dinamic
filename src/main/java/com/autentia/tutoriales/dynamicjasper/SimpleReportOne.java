package com.autentia.tutoriales.dynamicjasper;

import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import java.awt.Color;
import java.util.Date;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Ejemplo sencillo que muestra la configuración básica de un informe y sus
 * columnas
 *
 * @author Autentia
 */
public class SimpleReportOne extends ReportBase {

    public static void main(String[] args) throws Exception {
        SimpleReportOne simpleReportOne = new SimpleReportOne();
        simpleReportOne.generateReport();
        simpleReportOne.exportToJRXML();
        JasperViewer.viewReport(simpleReportOne.jp);	//finally display the report report
//        JasperDesignViewer.viewReportDesign(simpleReportOne.jr);
    }

    public DynamicReport buildReport() throws Exception {

        FastReportBuilder drb = new FastReportBuilder();
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setTextColor(Color.BLUE);
        styleColumns.setBorder(Border.PEN_1_POINT());
        drb.addColumn("Name", "name", String.class.getName(), 30)
                .addColumn("Address", "address", String.class.getName(), 30)
                .addColumn("Zip Code", "zipcode", String.class.getName(), 60, true)
                .addColumn("Country", "country", String.class.getName(), 60)
                .addColumn("visible", "visible", String.class.getName(), 50)
                .setTitle("Primer informe con Dynamic Jasper")
                .setSubtitle("Ha sido generado " + new Date())
                .setPrintBackgroundOnOddRows(true)
                .setUseFullPageWidth(true)
                //                .setUseFullPageWidth(true);
                .setColspan(2, 2, "Estimated");
//        drb.setColspan(1, 2, "Agrupado");
        return drb.build();

    }

}
