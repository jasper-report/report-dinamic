package com.autentia.tutoriales.dynamicjasper;

import ar.com.fdvs.dj.core.DJConstants;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.chart.DJChart;
import ar.com.fdvs.dj.domain.chart.DJChartOptions;
import ar.com.fdvs.dj.domain.chart.builder.DJBarChartBuilder;
import ar.com.fdvs.dj.domain.chart.plot.DJAxisFormat;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.GroupLayout;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import java.awt.Color;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.view.JasperViewer;
import org.jfree.chart.plot.PlotOrientation;

public class SimpleReportTemplate extends ReportBase {

    public static void main(String[] args) throws Exception {
        SimpleReportTemplate reportTemplate = new SimpleReportTemplate();
        reportTemplate.generateReport();
//        reportTemplate.exportToJRXML();
//        reportTemplate.exportToExcel(reportTemplate.jp);
        JasperViewer.viewReport(reportTemplate.jp);	//finally display the report report
//        JasperDesignViewer.viewReportDesign(reportTemplate.jr);

    }

    @Override
    public DynamicReport buildReport() throws Exception {
        params = populateParametersMap("Informe en base a una plantilla");
        return initMainReport();
    }

    private Map<String, String> populateParametersMap(String title) {
        final Map<String, String> parameters = new HashMap();
        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
        parameters.put("REPORT_LOGO", new File(pathLogo).getPath());
        parameters.put("REPORT_TITLE", title);
        return parameters;
    }

    public static DynamicReport initMainReport() throws Exception {
//        FastReportBuilder report = new FastReportBuilder();
        DynamicReportBuilder report = new DynamicReportBuilder();
        report.setTitle("EQUIPOS");
        report.setMargins(1, 1, 1, 1);
        report.setTemplateFile("dynamicJasperReportTemplate.jrxml");
        final String[] columns = {"name", "address", "zipcode", "country", "visible", "cantidad"};
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setTextColor(Color.BLUE);
        styleColumns.setBorder(Border.PEN_1_POINT());
        for (int i = 0; i < columns.length; i++) {
            String name = columns[i];
            AbstractColumn c1 = null;
            if (i == 5) {
                c1 = ColumnBuilder.getInstance()
                        .setColumnProperty(name, Integer.class.getName())
                        .setTitle(name)
                        //                        .setWidth(50)
                        .setStyle(styleColumns)
                        .build();
            } else {
                c1 = ColumnBuilder.getInstance()
                        .setColumnProperty(name, String.class.getName())
                        .setTitle(name)
                        //                        .setWidth(50)
                        .setStyle(styleColumns)
                        .build();
            }

            report.addColumn(c1);
        }
        report.setUseFullPageWidth(true);
//        report.setPrintColumnNames(true);
        Style detailStyle = new Style();

        Style headerStyle = new Style();
        headerStyle.setFont(Font.ARIAL_MEDIUM_BOLD);
        headerStyle.setBorderBottom(Border.PEN_1_POINT());
        headerStyle.setBackgroundColor(Color.gray);
        headerStyle.setTextColor(Color.white);
        headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
        headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
        headerStyle.setTransparency(Transparency.OPAQUE);

        Style headerVariables = new Style();
        headerVariables.setFont(Font.ARIAL_MEDIUM_BOLD);
        //		headerVariables.setBorderBottom(Border.THIN());
        headerVariables.setHorizontalAlign(HorizontalAlign.RIGHT);
        headerVariables.setVerticalAlign(VerticalAlign.MIDDLE);

        Style titleStyle = new Style();
        titleStyle.setFont(new Font(18, Font._FONT_VERDANA, true));
        Style importeStyle = new Style();
        importeStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
        Style oddRowStyle = new Style();
        oddRowStyle.setBorder(Border.NO_BORDER());
        oddRowStyle.setBackgroundColor(Color.LIGHT_GRAY);
        oddRowStyle.setTransparency(Transparency.OPAQUE);
//        report.setIgnorePagination(true); // para excel una sola pagina (hoja)
//        report.setColspan(2, 2, "Union por fin");
//        grafico(report, PlotOrientation.VERTICAL);
//        rb.setPrintColumnNames(true); //Show the column names on the top of every page
        GroupBuilder gb1 = new GroupBuilder();
        DJGroup g1 = gb1.setCriteriaColumn((PropertyColumn) report.getColumn(0))
//                .addHeaderVariable(report.getColumn(1), DJCalculation.SUM, headerVariables) // tell the group place a variable footer of the column "columnAmount" with the SUM of allvalues of the columnAmount in this group.
                .setGroupLayout(GroupLayout.VALUE_IN_HEADER) // tells the group how to be shown, there are manyposibilities, see the GroupLayout for more.
                .build();

        GroupBuilder gb2 = new GroupBuilder(); // Create another group (using another column as criteria)
        DJGroup g2 = gb1.setCriteriaColumn((PropertyColumn) report.getColumn(3))
                .addFooterVariable(report.getColumn(3), DJCalculation.SUM, headerVariables) // tell the group place a variable footer of the column "columnAmount" with the SUM of allvalues of the columnAmount in this group.
                .addFooterVariable(report.getColumn(4), DJCalculation.SUM, headerVariables) // idem for the columnaQuantity column
                .setGroupLayout(GroupLayout.VALUE_IN_HEADER) // tells the group how to be shown, there are manyposibilities, see the GroupLayout for more.
                .build();
        report.addGroup(g2);
//        report.addGroup(g1);
        return report.build();
    }

    public static void grafico(FastReportBuilder drb, PlotOrientation posicion) {
        DJAxisFormat categoryAxisFormat = new DJAxisFormat("Nombre del HRT");
        categoryAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelMask("");
        categoryAxisFormat.setLineColor(Color.DARK_GRAY);

        DJAxisFormat valueAxisFormat = new DJAxisFormat("Cantidad de supervisiones");
        valueAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelMask("#,##0.0");
        valueAxisFormat.setLineColor(Color.DARK_GRAY);

        DJChart djChart = new DJBarChartBuilder()
                //chart     
                .setX(20)
                .setY(10)
                .setWidth(500)
                .setHeight(250)
                .setCentered(false)
                .setBackColor(Color.LIGHT_GRAY)
                .setShowLegend(true)
                .setPosition(DJChartOptions.POSITION_FOOTER)
                .setTitle("Resumen de supervision HRT")
                .setTitleColor(Color.DARK_GRAY)
                .setTitleFont(Font.ARIAL_BIG_BOLD)
                //                .setSubtitle("subtitle")
                .setSubtitleColor(Color.DARK_GRAY)
                .setSubtitleFont(Font.COURIER_NEW_BIG_BOLD)
                .setLegendColor(Color.DARK_GRAY)
                .setLegendFont(Font.COURIER_NEW_MEDIUM_BOLD)
                .setLegendBackgroundColor(Color.WHITE)
                .setLegendPosition(DJChartOptions.EDGE_BOTTOM)
                .setTitlePosition(DJChartOptions.EDGE_TOP)
                .setLineStyle(DJChartOptions.LINE_STYLE_DOTTED)
                .setLineWidth(1)
                .setLineColor(Color.DARK_GRAY)
                .setPadding(5)
                //dataset
                .setOrientation(posicion)
                .setCategory((PropertyColumn) drb.getColumn(1))
                .addSerie(drb.getColumn(5))
                //plot
                //                .setXOffset(10)
                //                .setyOffset(10)
                .setShowLabels(false)
                .setCategoryAxisFormat(categoryAxisFormat)
                .setValueAxisFormat(valueAxisFormat)
                .build();
        drb.addChart(djChart);
    }

    public static FastReportBuilder initSubreport() throws Exception {
        FastReportBuilder report = new FastReportBuilder();
        final String[] columns = {"name", "address", "zipcode", "country"};
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setTextColor(Color.BLUE);
        styleColumns.setBorder(Border.PEN_1_POINT());
        for (int i = 0; i < columns.length; i++) {
            String name = columns[i];
            AbstractColumn c1 = ColumnBuilder.getInstance()
                    .setColumnProperty(name, String.class.getName())
                    .setTitle(name)
                    .setWidth(50)
                    .setStyle(styleColumns)
                    .build();
            report.addColumn(c1);
        }
        report.setUseFullPageWidth(true);
        report.setColspan(2, 2, "Union por fin");
        return report;
    }

    private static DynamicReport definirSubreport(FastReportBuilder report, FastReportBuilder subreport, String atributoColeccion) throws Exception {
//        report.addField(atributoColeccion, claseColeccion);
        report.addGroups(1).setGroupLayout(1, GroupLayout.EMPTY);
        report.addSubreportInGroupFooter(1, subreport.build(),
                new ClassicLayoutManager(), atributoColeccion,
                DJConstants.DATA_SOURCE_ORIGIN_FIELD,
                DJConstants.DATA_SOURCE_TYPE_COLLECTION);

        report.setUseFullPageWidth(true);

        return report.build();
    }

}
