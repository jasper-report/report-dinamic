/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.LayoutManager;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.chart.DJChart;
import ar.com.fdvs.dj.domain.chart.DJChartOptions;
import ar.com.fdvs.dj.domain.chart.builder.DJBarChartBuilder;
import ar.com.fdvs.dj.domain.chart.plot.DJAxisFormat;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.GroupLayout;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import ar.com.fdvs.dj.util.SortUtils;
import static com.autentia.tutoriales.dynamicjasper.ReportBase.log;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author Martin Pilar <mpilarcastillejo@gmail.com>
 */
public class UtilReportWeb {

    private Map<String, Integer> listaCabezera = new LinkedHashMap<>();
    private Map params = new HashMap();
    private String rutaPlantilla;
    private Collection<Map<String, Object>> data = new ArrayList<>();
    private DynamicReport dr;
    private JasperReport jr;
    public FastReportBuilder builder;

    /**
     * inyectar dato necesario del reporte
     *
     * @param listaCabezera
     * @param data
     * @param params
     * @param rutaPlantillaJrxml
     */
    public UtilReportWeb(Map<String, Integer> listaCabezera, Collection<Map<String, Object>> data, Map<String, String> params, String rutaPlantillaJrxml) {
        this.listaCabezera = listaCabezera;
        this.data = data;
        this.params = params;
        this.rutaPlantilla = rutaPlantillaJrxml;
    }

    /**
     * Construir el reporte indicando si hay grafico si es con grafico true y se
     * requiere los demas parametros caso contrario null
     *
     * @param conGrafico
     * @param titulo
     * @param categoria
     * @param serie
     * @param filaCategoria
     * @param filaSerie
     */
    public void ConstruirReporteExcel(boolean conGrafico, String titulo, String categoria, String serie, Integer filaCategoria, Integer filaSerie, PlotOrientation posicion) {
        try {
            obtenerReporte(conGrafico, titulo, categoria, serie, filaCategoria, filaSerie, posicion, true);
        } catch (Exception ex) {
            log.error(ex);
        }
    }

    /**
     * Construir el reporte indicando si hay grafico si es con grafico true y se
     * requiere los demas parametros caso contrario null
     *
     * @param conGrafico
     * @param titulo
     * @param categoria
     * @param serie
     * @param filaCategoria
     * @param filaSerie
     */
    public void ConstruirReportePDF(boolean conGrafico, String titulo, String categoria, String serie, Integer filaCategoria, Integer filaSerie, PlotOrientation posicion) {
        try {
            obtenerReporte(conGrafico, titulo, categoria, serie, filaCategoria, filaSerie, posicion, false);
        } catch (Exception ex) {
            log.error(ex);
        }
    }

    /**
     * generar el reporte
     */
    public JasperPrint generarReporte() {
        try {
            dr = builder.build();
            JRDataSource ds = getData();
            jr = DynamicJasperHelper.generateJasperReport(dr, getLayoutManager(), params);
            JasperPrint jp;
            if (ds != null) {
                jp = JasperFillManager.fillReport(jr, params, ds);
            } else {
                jp = JasperFillManager.fillReport(jr, params);
                log.debug("Exporting the report (pdf, xls, etc)");
            }
            return jp;
        } catch (Exception ex) {
            log.error(ex);
        }
        return null;
    }

    private LayoutManager getLayoutManager() {
        return new ClassicLayoutManager();
    }

    /**
     * genrar el reporte
     *
     * @param conGrafico
     * @param titulo
     * @param categoria
     * @param serie
     * @param filaCategoria
     * @param filaSerie
     * @throws Exception
     */
    public void obtenerReporte(boolean conGrafico, String titulo, String categoria, String serie, Integer filaCategoria, Integer filaSerie, PlotOrientation posicion, boolean autosize) throws Exception {
        builder = new FastReportBuilder();
//        report.setTitle("EQUIPOS");
        builder.setMargins(10, 10, 10, 10);
        if (rutaPlantilla != null) {
            builder.setTemplateFile(rutaPlantilla);
        }
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setTextColor(Color.BLUE);
        styleColumns.setBorder(Border.PEN_1_POINT());
        AtomicInteger indice = new AtomicInteger(0);
        listaCabezera.entrySet().stream().map((columna) -> {
            String nombre = columna.getKey();
            Integer width = columna.getValue();
            AbstractColumn c1;
            if (indice.get() == filaSerie) {
                c1 = ColumnBuilder.getInstance()
                        .setColumnProperty(nombre, Object.class.getName())
                        .setTitle(nombre)
                        .setWidth(width)
                        .setFixedWidth(autosize)
                        .setStyle(estiloFila())
                        .setCustomExpression(convertirInteger(indice.get()))
                        .setHeaderStyle(estiloHeader())
                        .build();
            } else {
                c1 = ColumnBuilder.getInstance()
                        .setColumnProperty(nombre, Object.class.getName())
                        .setTitle(nombre)
                        .setWidth(width)
                        .setFixedWidth(autosize)
                        .setStyle(estiloFila())
                        .setHeaderStyle(estiloHeader())
                        .build();
            }
            return c1;
        }).forEachOrdered((c1) -> {
            builder.addColumn(c1);
            indice.incrementAndGet();
        });

//builder.setS
        builder.setUseFullPageWidth(true);
        builder.setPrintColumnNames(true);
//        builder.setIgnorePagination(true); // para excel una sola pagina (hoja)
//        report.setColspan(indiceInicialAgrupar, cantidadAgrupar, "A�O");
        if (conGrafico) {
            grafico(posicion, titulo, categoria, serie, filaCategoria, filaSerie);
        }
    }

    /**
     * obtener grafico
     *
     *
     * @param posicion
     * @param titulo
     * @param categoria
     * @param serie
     * @param filaCategoria
     * @param filaSerie
     */
    public void grafico(PlotOrientation posicion, String titulo, String categoria, String serie, Integer filaCategoria, Integer filaSerie) {
        DJAxisFormat categoryAxisFormat = new DJAxisFormat(categoria);
        categoryAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelMask("");
        categoryAxisFormat.setLineColor(Color.DARK_GRAY);

        DJAxisFormat valueAxisFormat = new DJAxisFormat(serie);
        valueAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelMask("#,##0.0");
        valueAxisFormat.setLineColor(Color.DARK_GRAY);

        DJChart djChart = new DJBarChartBuilder()
                //chart     
                .setX(20)
                .setY(10)
                .setWidth(500)
                .setHeight(350)
                .setCentered(false)
                .setBackColor(Color.LIGHT_GRAY)
                .setShowLegend(false)
                .setPosition(DJChartOptions.POSITION_FOOTER)
                .setTitle(titulo)
                .setTitleColor(Color.DARK_GRAY)
                .setTitleFont(Font.ARIAL_BIG_BOLD)
                //                .setSubtitle("subtitle")
                .setSubtitleColor(Color.DARK_GRAY)
                .setSubtitleFont(Font.COURIER_NEW_BIG_BOLD)
                .setLegendColor(Color.DARK_GRAY)
                .setLegendFont(Font.COURIER_NEW_MEDIUM_BOLD)
                .setLegendBackgroundColor(Color.WHITE)
                .setLegendPosition(DJChartOptions.EDGE_BOTTOM)
                .setTitlePosition(DJChartOptions.EDGE_TOP)
                .setLineStyle(DJChartOptions.LINE_STYLE_DOTTED)
                .setLineWidth(1)
                .setLineColor(Color.DARK_GRAY)
                .setPadding(5)
                //dataset
                .setOrientation(posicion)
                .setCategory((PropertyColumn) builder.getColumn(filaCategoria))
                .addSerie(builder.getColumn(filaSerie))
                //plot
                //                .setXOffset(10)
                //                .setyOffset(10)
                .setShowLabels(false)
                .setCategoryAxisFormat(categoryAxisFormat)
                .setValueAxisFormat(valueAxisFormat)
                .build();
        builder.addChart(djChart);
    }

    /**
     * obtener data de la tabla
     *
     * @return
     */
    public JRDataSource getData() {
        Collection<Map<String, Object>> dataOneReport = data;
        dataOneReport = SortUtils.sortCollection(dataOneReport, dr.getColumns());
        return new JRBeanCollectionDataSource(dataOneReport);
    }

    /**
     * obtener estilo de cabezera
     *
     * @return
     */
    public Style estiloHeader() {
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        Color c = new Color(225, 35, 40);
        styleColumns.setTextColor(Color.WHITE);
        styleColumns.setBackgroundColor(c);
        styleColumns.setBorderColor(Color.WHITE);
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setBorder(Border.THIN());
        styleColumns.setVerticalAlign(VerticalAlign.MIDDLE);
        styleColumns.setTransparent(false);
        return styleColumns;
    }

    /**
     * obtener estilo de fila
     *
     *
     * @return
     */
    public Style estiloFila() {
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
//        Color c = new Color(225, 35, 40);
//        styleColumns.setTextColor(Color.WHITE);
//        styleColumns.setBackgroundColor(c);
//        styleColumns.setBorderColor(Color.WHITE);
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setBorder(Border.THIN());
        styleColumns.setVerticalAlign(VerticalAlign.MIDDLE);
//        styleColumns.setTransparent(false);
        return styleColumns;
    }

    /**
     * unir reportes en base al jasperBase
     *
     * @param jasperPrintBase jasper al cual se le agregara los demas
     * jasperprints
     * @param jasperPrints
     * @return
     */
    public JasperPrint unirReporte(JasperPrint jasperPrintBase, JasperPrint... jasperPrints) {
        if (jasperPrintBase != null && jasperPrints != null && jasperPrints.length > 0) {
            for (JasperPrint jasperPrint1 : jasperPrints) {
                for (int j = 0; j < jasperPrint1.getPages().size(); j++) {
                    jasperPrintBase.addPage(jasperPrint1.getPages().get(j));
                }
            }
        }
        return jasperPrintBase;
    }

    /**
     * agrupar filas de la tabla
     *
     * @param columnaCriterio indice de la columna del cual se tomara el
     * criterio de agrupacion
     * @param indiceColumnaSumar indice de las columnas que se totalizaran
     * @param estilo
     */
    public void agruparColumnaPorFila(Style estilo, int columnaCriterio, int indiceColumnaSumar) {
//    public void agruparColumnaPorFila(Style estilo, int columnaCriterio, int... indiceColumnaSumar) {
//        if (indiceColumnaSumar != null && indiceColumnaSumar.length > 0) {
        GroupBuilder gb1 = new GroupBuilder();
        DJGroup g1 = gb1.build();
        gb1.setCriteriaColumn((PropertyColumn) builder.getColumn(columnaCriterio));
//            for (int i : indiceColumnaSumar) {
        gb1.addFooterVariable(builder.getColumn(indiceColumnaSumar), DJCalculation.SUM, estilo);
        builder.addGlobalFooterVariable(builder.getColumn(indiceColumnaSumar), DJCalculation.SUM, estilo);
//            }
        gb1.setGroupLayout(GroupLayout.VALUE_FOR_EACH);
        builder.addGroup(g1);
//        }
    }

    /**
     * convetir campo a integer para poder calcular
     *
     * @param indicecolumna
     * @return
     */
    private CustomExpression convertirInteger(int indicecolumna) {
        return new CustomExpression() {
            @Override
            public Integer evaluate(Map fields, Map variables, Map parameters) {
                Integer amount = (Integer) listaCabezera.values().toArray()[indicecolumna];
                return amount;
            }

            @Override
            public String getClassName() {
                return Integer.class.getName();
            }
        };
    }

}
