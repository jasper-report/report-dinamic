package com.autentia.tutoriales.dynamicjasper;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.LayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.util.SortUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class ReportBase {

    protected static final Log log = LogFactory.getLog(ReportBase.class);
    protected JasperPrint jp;
    protected JasperReport jr;
    protected Map params = new HashMap();
    protected DynamicReport dr;

    public abstract DynamicReport buildReport() throws Exception;

    public void generateReport() throws Exception {

        dr = buildReport();

        /**
         * Ontenemos la fuente de datos en base a una colleccion de objetos
         */
        JRDataSource ds = getDataSource();

        /**
         * Creamos el objeto JasperReport que pasamos como parametro a
         * DynamicReport,junto con una nueva instancia de ClassicLayoutManager y
         * el JRDataSource
         */
        jr = DynamicJasperHelper.generateJasperReport(dr, getLayoutManager(), params);

        /**
         * Creamos el objeto que imprimiremos pasando como parametro el
         * JasperReport object, y el JRDataSource
         */
        log.debug("Filling the report");
        if (ds != null) {
            jp = JasperFillManager.fillReport(jr, params, ds);
        } else {
            jp = JasperFillManager.fillReport(jr, params);
            log.debug("Filling done!");
            log.debug("Exporting the report (pdf, xls, etc)");
        }

        exportReport();

        log.debug("test finished");

    }

    protected LayoutManager getLayoutManager() {
        return new ClassicLayoutManager();
    }

    protected void exportReport() throws Exception {

        final String path = System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".pdf";

        log.debug("Exporing report to: " + path);
        JRPdfExporter exporter = new JRPdfExporter();
        File outputFile = new File(path);
        File parentFile = outputFile.getParentFile();
        if (parentFile != null) {
            parentFile.mkdirs();
        }

        FileOutputStream fos = new FileOutputStream(outputFile);

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);

        exporter.exportReport();
        log.debug("Report exported: " + path);

    }

    protected void exportToJRXML() throws Exception {
        if (this.jr != null) {
            DynamicJasperHelper.generateJRXML(this.jr, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".jrxml");

        } else {
            DynamicJasperHelper.generateJRXML(this.dr, this.getLayoutManager(), this.params, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".jrxml");
        }
    }

    @SuppressWarnings("unchecked")
    protected JRDataSource getDataSource() {
        Collection<Map<String, Object>> dataOneReport = getDataOneReportMock();
        dataOneReport = SortUtils.sortCollection(dataOneReport, dr.getColumns());
        //Create a JRDataSource, the Collection used
        JRDataSource ds = new JRBeanCollectionDataSource(dataOneReport);
        return ds;

    }

    protected Collection<Map<String, Object>> getDataOneReportMock() {
        Collection<Map<String, Object>> dataResult = new ArrayList<>();
        Map<String, Object> elementToInclude;
        for (Integer i = 0; i < 500; i++) {
            elementToInclude = new HashMap<>();
            elementToInclude.put("name", "name" + i);
            elementToInclude.put("address", "name" + i);
            elementToInclude.put("zipcode", "name" + i);
            elementToInclude.put("country", "name" + i);
            elementToInclude.put("visible", "name" + i);
            elementToInclude.put("cantidad", i);
            dataResult.add(elementToInclude);
        }

        return dataResult;
    }

    public void exportToExcel(JasperPrint jp) throws Exception {

//Creamos una instancia del objeto JRXlsExplorer que se encuentra en la librer�a jasperreports-X.X.X.jar
        JRXlsExporter exporter = new JRXlsExporter();
//Si queremos definir varios Sheet lo hacemos en un array de cadenas que posteriormente la a�adiremos por par�metros.
        String[] sheetNames = {"Hoja 1"};

//Creamos el fichero de salida donde exportaremos el informe a Excel
        File outputFile = new File("D://" + "exportacionEXCEL.xls");
        FileOutputStream fos = new FileOutputStream(outputFile);

//Empezamos a definir los par�metros de la exportacion
//Indicamos el objeto JasperPrint que deseamos exportar
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
//Indicamos el fichero donde vamos a exportar el informe
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos); //and output stream

//Excel specific parameter
//Indicamos si queremos una p�gina por Sheet
        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
//Indicamos si deseamos eliminar los espacios vac�os entre filas
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
//Indicamos si quremos mostrar una p�gina en blanco como fondo
        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

//Definimos los nombres de los Sheet que es el Array de String que definimos arriba
        exporter.setParameter(JRXlsExporterParameter.SHEET_NAMES, sheetNames);

//Usamos la funci�n exportReport para crear el nuevo Excel.
        exporter.exportReport();
        log.debug("Report exported: " + System.getProperty("user.dir") + "/src/main/resources" + "exportacionEXCEL.xls");
    }

    public static class MapPersonal<T> {

        Map<String, T> propiedad;

    }
}
