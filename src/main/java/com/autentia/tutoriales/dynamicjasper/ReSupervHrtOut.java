/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author calidad
 */
public class ReSupervHrtOut {

    private Integer id;
    private Integer idHrt;
    private String razonSocial;
    private List<ReSupervisionesAnioOut> supervisionesPorAnio;
    private BigInteger totalSuperv;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdHrt() {
        return idHrt;
    }

    public void setIdHrt(Integer idHrt) {
        this.idHrt = idHrt;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public List<ReSupervisionesAnioOut> getSupervisionesPorAnio() {
        return supervisionesPorAnio;
    }

    public void setSupervisionesPorAnio(List<ReSupervisionesAnioOut> supervisionesPorAnio) {
        this.supervisionesPorAnio = supervisionesPorAnio;
    }

    public BigInteger getTotalSuperv() {
        return totalSuperv;
    }

    public void setTotalSuperv(BigInteger totalSuperv) {
        this.totalSuperv = totalSuperv;
    }

    public ReSupervHrtOut(Integer id, Integer idHrt, String razonSocial, List<ReSupervisionesAnioOut> supervisionesPorAnio, BigInteger totalSuperv) {
        this.id = id;
        this.idHrt = idHrt;
        this.razonSocial = razonSocial;
        this.supervisionesPorAnio = supervisionesPorAnio;
        this.totalSuperv = totalSuperv;
    }

}
