///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.autentia.tutoriales.dynamicjasper;
//
//import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import net.sf.jasperreports.view.JasperViewer;
//
///**
// *
// * @author Martin Pilar <mpilarcastillejo@gmail.com>
// */
//public class Reporte8final extends UtilReport {
//
//    public Reporte8final(List<String> listaCabezera, Collection<Map<String, Object>> data, Map<String, String> params) {
//        super(listaCabezera, data, params);
//    }
//
//    public static void main(String[] args) throws Exception {
//        List<ReSupervHrtOut> reporte8Lista = obtenerDataServicio();
//        List<Map<String, Object>> listaData = obtenerData(reporte8Lista);
//        List<String> listaCabezera = obtenerListaCabezera(reporte8Lista);
//        Map<String, String> params = obtenerParametros("Reporte 8");
//        Reporte8final report = new Reporte8final(listaCabezera, listaData, params);
//        FastReportBuilder builder = report.ConstruirReporte(true, "Grafico", "Categoria", "Serie");
//        builder.setColspan(2, 10, "A�O");
//        report.generarReporte(builder);
//        report.exportToExcel(report.jp);
//        JasperViewer.viewReport(report.jp);
////        reportTemplate.exportToJRXML();
////        reportTemplate.exportToExcel(reportTemplate.jp);
////        JasperDesignViewer.viewReportDesign(reportTemplate.jr);
//    }
//
//    public static Map<String, String> obtenerParametros(String title) {
//        final Map<String, String> parameters = new HashMap();
//        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
////        URL urllogo = getClass().getResource("/logo-mimp.jpg");
//        parameters.put("titulo", title);
//        parameters.put("logo", pathLogo);
//        return parameters;
//    }
//
//    private static List<ReSupervHrtOut> obtenerDataServicio() {
//        List<ReSupervHrtOut> reporte8Lista = new ArrayList();
//        for (Integer i = 0; i < 10; i++) {
//            List<ReSupervisionesAnioOut> anios = new ArrayList();
//            for (Integer j = 0; j < 10; j++) {
//                anios.add(new ReSupervisionesAnioOut("201" + j, i));
//            }
//            ReSupervHrtOut temp = new ReSupervHrtOut(i, i * 5, "razon" + i, anios, i * 8);
//            reporte8Lista.add(temp);
//        }
//        return reporte8Lista;
//    }
//
//    private static List<Map<String, Object>> obtenerData(List<ReSupervHrtOut> reporte8Lista) {
//        List<Map<String, Object>> data = new ArrayList<>();
//        for (ReSupervHrtOut supe : reporte8Lista) {
//            Map<String, Object> dat = new HashMap<>();
//            dat.put("CODIGO", supe.getIdHrt().toString());
//            dat.put("HRT", supe.getRazonSocial());
//            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
//                dat.put(nombreAnio.getAnio(), nombreAnio.getCantSupervisiones().toString());
//            }
//            dat.put("TOTAL", supe.getTotalSuperv().toString());
//            data.add(dat);
//        }
//        return data;
//    }
//
//    private static List<String> obtenerListaCabezera(List<ReSupervHrtOut> tempListaSupervision) {
//        List<String> listaCabezera = new ArrayList<>();
//        for (ReSupervHrtOut supe : tempListaSupervision) {
//            listaCabezera.add("CODIGO");
//            listaCabezera.add("HRT");
//            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
//                listaCabezera.add(nombreAnio.getAnio());
//            }
//            listaCabezera.add("TOTAL");
//            break;
//        }
//        return listaCabezera;
//    }
//}
