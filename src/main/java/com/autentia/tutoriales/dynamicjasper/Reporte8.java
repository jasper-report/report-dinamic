/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autentia.tutoriales.dynamicjasper;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.LayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.chart.DJChart;
import ar.com.fdvs.dj.domain.chart.DJChartOptions;
import ar.com.fdvs.dj.domain.chart.builder.DJBarChartBuilder;
import ar.com.fdvs.dj.domain.chart.plot.DJAxisFormat;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import ar.com.fdvs.dj.util.SortUtils;
import static com.autentia.tutoriales.dynamicjasper.ReportBase.log;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JasperViewer;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author Martin Pilar <mpilarcastillejo@gmail.com>
 */
public class Reporte8 {

    public static JasperPrint jp;
    private static JasperReport jr;
    private static Map params = new HashMap();
    private static DynamicReport dr;
    private static List<String> listaCabezeraReporte8 = null;
    private static Map<String, String> listaCabezera = new HashMap<>();
    private static List<Map<String, String>> dataReporte = new ArrayList<>();
    private static int cantidadAgrupar = 0;

    private String nombre = System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".pdf";

    public void generarReporte() {
        try {
            obtenerData();
            FastReportBuilder builder = obtenerReporte();
//            grafico(builder, PlotOrientation.HORIZONTAL);
            dr = builder.build();
            JRDataSource ds = getDataSource();
            params = populateParametersMap("Demo");
            jr = DynamicJasperHelper.generateJasperReport(dr, getLayoutManager(), params);
            if (ds != null) {
                jp = JasperFillManager.fillReport(jr, params, ds);
            } else {
                jp = JasperFillManager.fillReport(jr, params);
                log.debug("Filling done!");
                log.debug("Exporting the report (pdf, xls, etc)");
            }
            exportReport();
            exportToJRXML();
        } catch (Exception ex) {
            Logger.getLogger(Reporte8.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void exportReport() throws Exception {

        String path = obtenerNOmbre();
        log.debug("Exporing report to: " + path);
        JRPdfExporter exporter = new JRPdfExporter();
        File outputFile = new File(path);
        File parentFile = outputFile.getParentFile();
        if (parentFile != null) {
            parentFile.mkdirs();
        }

        FileOutputStream fos = new FileOutputStream(outputFile);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
        exporter.exportReport();
        log.debug("Report exported: " + path);
    }

    public String obtenerNOmbre() {
        return nombre;
    }

    public FastReportBuilder obtenerReporte() throws Exception {
        FastReportBuilder report = new FastReportBuilder();
        report.setTitle("EQUIPOS");
        report.setMargins(1, 1, 1, 1);
        report.setTemplateFile("reporte8.jrxml");
        Style styleColumns = new Style();
        styleColumns.setHorizontalAlign(HorizontalAlign.CENTER);
        styleColumns.setTextColor(Color.BLUE);
        styleColumns.setBorder(Border.PEN_1_POINT());
        for (Map.Entry<String, String> entry : listaCabezera.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            AbstractColumn c1 = ColumnBuilder.getInstance()
                    .setColumnProperty(key, Object.class.getName())
                    .setTitle(key)
                    .setWidth(50)
                    .setStyle(styleColumns)
                    .build();
            report.addColumn(c1);
        }

        report.setUseFullPageWidth(true);
        report.setPrintColumnNames(true);
//        report.setIgnorePagination(true); // para excel una sola pagina (hoja)
        report.setColspan(2, cantidadAgrupar, "A�O");
//        grafico(report, PlotOrientation.VERTICAL);
        return report;
    }

    private Map<String, Object> populateParametersMap(String title) {
        final Map<String, Object> parameters = new HashMap<String, Object>();
        final String pathLogo = System.getProperty("user.dir") + "/src/main/resources/autentialogo.png";
//        URL urllogo = getClass().getResource("/logo-mimp.jpg");
        parameters.put("titulo", title);
        parameters.put("logo", pathLogo);
        return parameters;
    }

    private List<Map<String, Object>> obtenerData() {
        List<ReSupervHrtOut> reporte8Lista = new ArrayList();
        for (Integer i = 0; i < 10; i++) {
            List<ReSupervisionesAnioOut> anios = new ArrayList();
            for (Integer j = 0; j < 10; j++) {
                anios.add(new ReSupervisionesAnioOut("201" + j, i));
            }
            ReSupervHrtOut temp = new ReSupervHrtOut(i, i * 5, "razon" + i, anios, new BigInteger("8"));
            reporte8Lista.add(temp);
        }
        List<Map<String, Object>> data = new ArrayList<>();
        for (ReSupervHrtOut supe : reporte8Lista) {
            Map<String, Object> dat = new HashMap<>();
            dat.put("Codigo", supe.getIdHrt().toString());
            dat.put("Nombre HRT", supe.getRazonSocial());
            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
                dat.put(nombreAnio.getAnio(), nombreAnio.getCantSupervisiones().toString());
            }
            dat.put("Total", supe.getTotalSuperv().toString());
            data.add(dat);
        }

        obtenerListaAnio(reporte8Lista);
        return data;
    }

    /**
     * Generar columnas dinamicas
     *
     * @param tempListaSupervision
     */
    private void obtenerListaAnio(List<ReSupervHrtOut> tempListaSupervision) {
        cantidadAgrupar = 0;
        for (ReSupervHrtOut supe : tempListaSupervision) {
            listaCabezera.put("Codigo", String.class.getName());
            listaCabezera.put("Nombre HRT", String.class.getName());
            for (ReSupervisionesAnioOut nombreAnio : supe.getSupervisionesPorAnio()) {
                listaCabezera.put(nombreAnio.getAnio(), Integer.class.getName());
                cantidadAgrupar++;
            }
            listaCabezera.put("Total", String.class.getName());
            break;
        }
    }

    private JRDataSource getDataSource() {
        Collection<Map<String, Object>> dataOneReport = getDataOneReportMock();
        dataOneReport = SortUtils.sortCollection(dataOneReport, dr.getColumns());
        //Create a JRDataSource, the Collection used
        JRDataSource ds = new JRBeanCollectionDataSource(dataOneReport);
        return ds;

    }

    private Collection<Map<String, Object>> getDataOneReportMock() {
        Collection<Map<String, Object>> dataResult = new ArrayList();
        dataResult.addAll(obtenerData());
        return dataResult;
    }

    private LayoutManager getLayoutManager() {
        return new ClassicLayoutManager();
    }

    public void exportToJRXML() throws Exception {
        if (jr != null) {
            DynamicJasperHelper.generateJRXML(jr, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".jrxml");

        } else {
            DynamicJasperHelper.generateJRXML(dr, getLayoutManager(), params, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getCanonicalName() + ".jrxml");
        }
    }

    public static void main(String[] args) {
        try {
            Reporte8 r = new Reporte8();
            r.generarReporte();
            r.exportToJRXML();
            JasperViewer.viewReport(Reporte8.jp);
        } catch (Exception ex) {
            Logger.getLogger(Reporte8.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void grafico(FastReportBuilder drb, PlotOrientation posicion) {
        DJAxisFormat categoryAxisFormat = new DJAxisFormat("Nombre del HRT");
        categoryAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        categoryAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        categoryAxisFormat.setTickLabelMask("");
        categoryAxisFormat.setLineColor(Color.DARK_GRAY);

        DJAxisFormat valueAxisFormat = new DJAxisFormat("Cantidad de supervisiones");
        valueAxisFormat.setLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelFont(Font.ARIAL_SMALL);
        valueAxisFormat.setTickLabelColor(Color.DARK_GRAY);
        valueAxisFormat.setTickLabelMask("#,##0.0");
        valueAxisFormat.setLineColor(Color.DARK_GRAY);

        DJChart djChart = new DJBarChartBuilder()
                //chart     
                .setX(20)
                .setY(10)
                .setWidth(500)
                .setHeight(250)
                .setCentered(false)
                .setBackColor(Color.LIGHT_GRAY)
                .setShowLegend(true)
                .setPosition(DJChartOptions.POSITION_FOOTER)
                .setTitle("Resumen de supervision HRT")
                .setTitleColor(Color.DARK_GRAY)
                .setTitleFont(Font.ARIAL_BIG_BOLD)
                //                .setSubtitle("subtitle")
                .setSubtitleColor(Color.DARK_GRAY)
                .setSubtitleFont(Font.COURIER_NEW_BIG_BOLD)
                .setLegendColor(Color.DARK_GRAY)
                .setLegendFont(Font.COURIER_NEW_MEDIUM_BOLD)
                .setLegendBackgroundColor(Color.WHITE)
                .setLegendPosition(DJChartOptions.EDGE_BOTTOM)
                .setTitlePosition(DJChartOptions.EDGE_TOP)
                .setLineStyle(DJChartOptions.LINE_STYLE_DOTTED)
                .setLineWidth(1)
                .setLineColor(Color.DARK_GRAY)
                .setPadding(5)
                //dataset
                .setOrientation(posicion)
                .setCategory((PropertyColumn) drb.getColumn(1))
                .addSerie(drb.getColumn(12))
                //plot
                //                .setXOffset(10)
                //                .setyOffset(10)
                .setShowLabels(false)
                .setCategoryAxisFormat(categoryAxisFormat)
                .setValueAxisFormat(valueAxisFormat)
                .build();
        drb.addChart(djChart);
    }

}
